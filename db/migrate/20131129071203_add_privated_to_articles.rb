class AddPrivatedToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :privated, :boolean, :default => false
  end
end
