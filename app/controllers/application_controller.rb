class ApplicationController < ActionController::Base
  before_filter :load_sidebar_resources
  
  protect_from_forgery
  
  private
  
  def load_sidebar_resources
    @course_categories = Category.for_course.order("id DESC")
    @article_categories = Category.for_article.order("id DESC")
    @tags = Tag.order("id DESC")
  end
end
