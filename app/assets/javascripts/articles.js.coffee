# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  tag_it = (tags, element) ->
    $this = tags
    $("span", $this).each ->
      $tag = $(this)
      $tag.click ->
        $tag.toggleClass "label-success"
        tag_list = ""
        $("span.label-success", $this).each ->
          tag_list += $(this).html() + " "

        element.val tag_list

  tag_it $("#all_tags_for_article"), $("#article_tag_list")


fileSelect = (btn, origin_file_field, new_file_field) ->
  btn.click ->
    origin_file_field.click()

  new_file_field.click ->
    origin_file_field.click()

  origin_file_field.change ->
    new_file_field.val origin_file_field.val()

fileSelect $(".browse_btn"), $("input[id=article_screenshot]"), $("#screenshotCover")
